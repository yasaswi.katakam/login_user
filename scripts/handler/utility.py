from pymongo import MongoClient


class Mongo_utility:
    @staticmethod
    def connection():
        client = MongoClient('143.110.191.155', 37217)
        database = client.yasaswi
        register_details = database.register_details
        return register_details

    @staticmethod
    def insert(register_details, data):
        register_details.insert(data)

    @staticmethod
    def finding(register_details, query):
        details = register_details.find(query)
        return details
