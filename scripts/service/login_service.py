from flask import Blueprint
from flask import render_template, request, flash
import bcrypt
from scripts.handler.login_handler import Mongobd

login_blueprint = Blueprint('blueprint_obj', __name__)
mongodb_object = Mongobd()


@login_blueprint.route('/login')
def login():
    return render_template('login.html')


@login_blueprint.route('/registration')
def register():
    return render_template('registration.html')


@login_blueprint.route('/registration_success')
def register_success():
    return render_template('registration_success.html')


@login_blueprint.route('/registration_data', methods=['POST'])
def registration_data():
    data = {"Name": request.form["name"],
            "Phone number": request.form["number"],
            "Mail id": request.form["mail"],
            "User Id": request.form["user_id"],
            "Password": bcrypt.hashpw((request.form["password"]).encode('utf8'), bcrypt.gensalt())}
    result = mongodb_object.insert(data, request.form["user_id"])
    if result == 1:
        flash("you have successfully registered.You can Login")
        return render_template('login.html')
    else:
        error = "User_id already exists"
        return render_template('registration.html', error=error)


@login_blueprint.route('/login_data', methods=['POST'])
def login_data():
    result = mongodb_object.find_obj(request.form["username"], request.form["pwd"])
    if result == 1:
        return render_template('login_success.html')
    elif result == 2:
        error = "Register here"
        return render_template('registration.html', error=error)
    else:
        error = "Please enter correct password"
        return render_template('login_fail.html', error=error)
