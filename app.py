from flask import Flask

from scripts.service.login_service import login_blueprint

app = Flask(__name__)
app.register_blueprint(login_blueprint)

if __name__ == '__main__':
    app.run(port=8200)
